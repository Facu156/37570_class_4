const fs = require('fs')

class ProductManager {
    constructor(pathFile) {
        this.id = 0
        this.id !== 0 ? this.id++ : ''
        this.path = pathFile       
    }
     async addProduct(obj) {
        const getDataInTxt = (fs.readFileSync(this.path, 'utf-8'))
        const arrData = getDataInTxt !== '' ? JSON.parse(getDataInTxt) : await fs.writeFileSync(this.path, JSON.stringify([{id: this.id, ...obj}], null, 2),
        console.log(`Producto creado con ID ${this.id}`))
        getDataInTxt !== '' ? (arrData.push({id: this.id,...obj}),
            await fs.writeFileSync(this.path, JSON.stringify(arrData, null, 2)),
            console.log(`Producto creado con ID ${this.id}`)
            )
            : ''
        this.id++      
     }
    async getProducts() {
        await console.log(JSON.parse(fs.readFileSync(this.path, 'utf-8'))) 
    }
    async getProductById(id) {
        const getDataInTxt = await fs.readFileSync(this.path, 'utf-8')
        const arr = []
        const fileParsed = JSON.parse(getDataInTxt)
        arr.push(...fileParsed)
        const arrayFilteredOut = arr.filter(el => el.id === id )
        console.log('readById', arrayFilteredOut)
    }
    async deleteById(id) {
        const getDataInTxt = await fs.readFileSync(this.path, 'utf-8')
        const arr = []
        const fileParsed = JSON.parse(getDataInTxt)
        arr.push(...fileParsed)
        const arrayFilteredOut = arr.filter(el => el.id !== id )
        console.log('Product deleted: ', arrayFilteredOut)
        return await fs.writeFileSync(this.path,JSON.stringify(arrayFilteredOut, null, 2))
    } 
}

const test = new ProductManager('./database.txt')

test.addProduct({title: 'titulo', description: 'descripcion', price: 20, thumbnail: 'https://storage.google.com/test/', code: 2213, stock: 5})
//test.addProduct({title: 'titulo1', description: 'descripcion2', price: 23, thumbnail: 'https://storage.google.com/test/', code: 2213, stock: 2})
//test.addProduct('titulo2', 'descripcion2', 25, 'https://storage.google.com/test/', 222313, 20)
//test.getProducts()
//test.getProductById(0)
//test.deleteById(1)
